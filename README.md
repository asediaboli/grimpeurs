# OCR - Projet 6 - Site web communautaire sur l'escalade

Ce projet est un exemple de site web communautaire dont l'objectif est de partager entre ses membres leurs connaissances des sites d'escalade.

# Fonctionnalités proposées

F1 : Un utilisateur doit pouvoir consulter les informations des sites d’escalades (secteurs, voies, longueurs).

F2 : Un utilisateur doit pouvoir faire une recherche à l’aide de plusieurs critères pour trouver un site de grimpe et consulter le résultat de cette recherche. Les critères peuvent être le lieu, la cotation, le nombre de secteurs, etc.

F3 : Un utilisateur doit pouvoir s’inscrire gratuitement sur le site.

F4 : Un utilisateur connecté doit pouvoir partager des informations sur un site d’escalade (secteurs, voies, longueurs, etc.).

F5 : Un utilisateur connecté doit pouvoir laisser des commentaires sur les pages des sites d’escalade.

F6 : Un membre de l'association doit pouvoir taguer un site d’escalade enregistré sur la plateforme comme « Officiel Les amis de l’escalade ».

F7 : Un membre de l'association doit pouvoir modifier et supprimer un commentaire.

F8 : Un utilisateur connecté doit pouvoir enregistrer dans son espace personnel les topos qu’ils possèdent et préciser si ces derniers sont disponibles pour être prêtés ou non. Un topo est défini par un nom, une description, un lieu et une date de parution.

F9 : Un utilisateur connecté doit pouvoir consulter la liste des topos disponibles par d’autres utilisateurs et faire une demande de réservation. La réservation n’inclut pas les notions de date de début et date de fin.

F10 : Un utilisateur connecté doit pouvoir accepter une demande de réservation. Si une réservation est acceptée, automatiquement le topo devient indisponible. L’utilisateur connecté pourra ensuite de nouveau changer le statut du topo en « disponible ». La plateforme se contente de mettre en contact les deux parties pour le prêt d’un topo (par échange de coordonnées).



# Déploiement de l'application

## Préparation de la base de données
Pour le fonctionnement normal de notre système, il faut créer une base de données et un utilisateur de cette base de données .
 
 Exécutez les commandes suivantes à ligne de commandes Postgres :
 
 Si vous n'avez pas d'utilisateur de Postgres
 
   `CREATE USER grimpeur WITH password '0000';` 
 
   `CREATE DATABASE grimpeurs WITH OWNER grimpeur;`
 
 Si vous avez déjà un utilisateur de Postgres
  
   `CREATE DATABASE grimpeurs WITH OWNER NOM_UTILISATEUR;`
   et apporter des modifications au fichier de configuration _src/main/resources/application.properties_
    
 
### Préchargement de données
Afin de permettre l'utilisation du site à l'issue de son démarrage, des données sont préchargées au démarrage du site.
Le framework Flyway se chargera de créer les tables de base de données et de les remplir de données au démarrage de l'application.

Le préchargement des données se déroule en 3 étapes.

* La première étape est la création du modèle de persistance dans la Base de Données.
Ces modèles de persistances sont présentes sous forme de statements SQL dans le fichier _src/main/resources/db/migration/V1__Init_DB.sql_.

* La deuxième est la création dans la base de données d'un utilisateur avec des privilèges d'administrateur.
Cet utilisateur est présent dans le fichier _src/main/resources/db/migration/V2__Add_admin.sql_.

    Administrateur pré-enregistré :

    Email :
`admin@admin.com`

    Mot de pass :
`admin`


* La troisième (optionnelle) est préchargement de jeu de données de démo.
Ces données sont présentes dans le fichier _src/main/resources/V3__Demo_data.sql_

    Pour démarrer l'application avec ces données :
   * Déplacez le fichier _V3__Demo_data.sql_ vers le répertoire _src/main/resources/db/migration_

   *    Exécutez les commandes à ligne de commandes Postgres :
         * `DROP database grimpeurs;` 
         * `CREATE DATABASE grimpeurs WITH OWNER NOM_UTILISATEUR;`

        


##  Déploiement avec ou sans conteneur web préinstallé
L'application peut être déployée de deux façons :
1) sous forme d'une application standalone intégrant un conteneur web (grâce à SpringBoot)
2) sous forme d'une webapp traditionnelle (__war__) à déployer dans le répertoire webapps d'un conteneur web (comme Tomcat, par exemple)

### Déploiement sans conteneur web 
C'est le mode de déploiement le plus simple car il ne nécessite pas l'installation préalable d'un conteneur web. Pour tester l'application, c'est le mode de déploiement préconisé.

La procédure est la suivante :
1) cloner le projet gitlab
2) S'assurer que la version de Java utilisée est la version 1.8 et que Maven est installé
3) Exécuter la ligne de commande dans le répertoire racine du projet : `mvn clean package spring-boot:run` 
4) Ouvrir un browser web à l'adresse http://localhost:8080

    
### Déploiement dans un conteneur web

Le format du package spécifié dans le fichier pom.xml est war. Une version du war produit à l'issue de l'étape de packaging par Maven est fournie dans le répertoire target, prêt pour déploiement dans un conteneur web.
Il est également possible de générer ce même package via la commande `mvn clean package`.

Si le package target/grimpeurs-0.0.1-SNAPSHOT.war est déployé dans un conteneur web(ex. Tomcat _déplacez ce fichier vers le répertoire dir_tomcat/webapps_), son URL sera (pour un Tomcat déployé en local disposant de sa configuration par défaut) http://localhost:8080/
 
## Jeu de donné de démo
En cas de lancement l'application avec le fichier _V3__Demo_data.sql_. Il y a déjà les utilisateurs enregistrés dans la base de données.

Email :`user@user.com` Mot de pass :`user`
 
Email :`user2@user.com` Mot de pass :`user`

Email :`membre@membre.com` Mot de pass :`membre`



# Description technique

L'application utilise les frameworks & projets suivants:

1) _SpringBoot 2.2.5.RELEASE_ (afin de faciliter la gestion des dépendances vis-à-vis des autres projets Spring)

2) _Spring MVC_ (afin de faciliter le développement des artéfacts 'web' par une approche déclarative par annotations)

3) _Spring Data_ (afin de faciliter le développement de la couche repository et l'utilisation de l'ORM Hibernate)

4) _Spring Security_ (pour faciliter la déclaration de droits d'accès aux ressources et les droits associés aux utilisateurs)

5) _PostgreSQL_ (un système de gestion de base de données relationnelle)

6) _Flyway_ (un outil de migration de base de données open source)

7) _Thymeleaf_ & _Thymeleaf Spring Security_ (pour faciliter l'écriture des templates de page)

8) _Bootstrap_ (pour faciliter le développement des composants (HTML/CSS) et le support du responsive)

9) _Commons BeanUtils_ (méthodes de commodité statique pour JavaBeans : pour instancier des beans, vérifier les types de propriétés de bean, copier des propriétés de bean, etc)

10) _QueryDsl_ (un framework qui permet la construction de requêtes de type SQL de type sécurisé pour plusieurs backends, y compris JPA, MongoDB et SQL en Java)


# Livrables attendus
L'ensemble des livrables attendus sont inclus dans le projet Gitlab :
1) Code source : répertoire _src_
2) Scripts SQL de création de la BdD et jeu de données de démo :
    * Creation des tables _src/main/resources/db/migration/V1__Init_DB.sql_
 
    * Creation d'un administrateur _src/main/resources/db/migration/V2__Add_admin.sql_

    * Jeu de donné de démo _src/main/resources/db/migration/V3__Demo_data.sql_
3) Documentation succincte : ce fichier _README.md_
4) Fichiers de configuration : _src/main/resources/application.properties_
