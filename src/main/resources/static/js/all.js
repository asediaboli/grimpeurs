function validateForm() {
    var x = document.forms["filter"]["inputcotation"].value;
    var list = ["3A", "3B", "3C", "4A", "4B", "4C", "5A", "5B", "5C", "6A", "6B", "6C", "7A", "7B", "7C", "8A", "8B", "8C", "9A", "9B", "9C", ""];
    if (!list.includes(x)) {
        $("#myModal").modal('show');
        return false;
    }
    return true;
}

function $_get(key) {
    const url = new URL(window.location.href);
    return url.searchParams.get(key) !== null;
}

$(document).ready(function () {
    if ($_get("exist")) {
        $("#myModal").modal('show');
    }
});
