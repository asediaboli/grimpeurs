INSERT INTO public.usr (id, active, date_creation, date_modified, email, nome, password, prenom)
VALUES (1, false, '2020-03-22 18:55:28.127000', '2020-03-22 18:55:28.127000', 'admin@admin.com', 'admin',
        '$2a$08$ePrH3lFBlbJpvRAbx/ZMv.zqQdapUf8Th6gRnD7Jwi5GUfv0ns6.a', 'admin');

SELECT pg_catalog.setval('usr_id_seq', 1, true);



INSERT INTO public.user_role (user_id, roles)
VALUES (1, 'ADMIN');
