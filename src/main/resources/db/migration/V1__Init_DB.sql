create table comment
(
    id            bigserial not null,
    date_creation timestamp,
    date_modified timestamp,
    text          varchar(2048),
    user_id       int8      not null,
    site_id       int8      not null,
    primary key (id)
);

create table longueur
(
    id            bigserial not null,
    date_creation timestamp,
    date_modified timestamp,
    description   varchar(2048),
    height        int4      not null,
    name          varchar(255),
    voie_id       int8      not null,
    primary key (id)
);

create table reservation
(
    id            bigserial not null,
    annuled       boolean default false,
    confirmed     boolean default false,
    date_creation timestamp,
    date_modified timestamp,
    dismiss       boolean default false,
    topo_id       int8      not null,
    user_id       int8      not null,
    primary key (id)
);

create table secteur
(
    id            bigserial not null,
    date_creation timestamp,
    date_modified timestamp,
    description   varchar(2048),
    name          varchar(255),
    site_id       int8      not null,
    primary key (id)
);

create table site
(
    id               bigserial not null,
    city             varchar(150),
    code_post        int4      not null,
    cotation_max     varchar(255),
    date_creation    timestamp,
    date_modified    timestamp,
    description      varchar(4096),
    name             varchar(255),
    numbers_comments int4,
    numbers_secteurs int4,
    numbers_voies    int4,
    official         boolean default false,
    region           varchar(150),
    street           varchar(255),
    createur_id      int8      not null,
    primary key (id)
);

create table topo
(
    id            bigserial not null,
    date_creation timestamp,
    date_modified timestamp,
    description   varchar(2048),
    ispublic      boolean default false,
    name          varchar(255),
    reserved      boolean default false,
    site_id       int8      not null,
    user_id       int8      not null,
    primary key (id)
);

create table user_role
(
    user_id int8 not null,
    roles   varchar(255)
);

create table usr
(
    id            bigserial not null,
    active        boolean   not null,
    date_creation timestamp,
    date_modified timestamp,
    email         varchar(255),
    nome          varchar(255),
    password      varchar(255),
    prenom        varchar(255),
    primary key (id)
);

create table voie
(
    id            bigserial not null,
    cotation      varchar(255),
    date_creation timestamp,
    date_modified timestamp,
    description   varchar(2046),
    name          varchar(255),
    secteur_id    int8      not null,
    primary key (id)
);

alter table usr
    add constraint UK_g9l96r670qkidthshajdtxrqf unique (email);

alter table comment
    add constraint FKgcgdcgly6u49hf4g8y2di3g4p foreign key (user_id) references usr;

alter table comment
    add constraint FK6jl05pq36bj9wvqvsoq3xbncg foreign key (site_id) references site;

alter table longueur
    add constraint FKro1y7gu1g630s7j7vaiksn6s5 foreign key (voie_id) references voie;

alter table reservation
    add constraint FKs6rhp8bjbn9p9imq1k5fcpb6a foreign key (topo_id) references topo;

alter table reservation
    add constraint FK8ty5kvnxkl7sqfbnekal3hpn foreign key (user_id) references usr;

alter table secteur
    add constraint FKc5crh1gx05vjasl4jusc1mgh2 foreign key (site_id) references site;

alter table site
    add constraint FKrn0wn3nwfa53n9b83xrn8kpll foreign key (createur_id) references usr;

alter table topo
    add constraint FKs9nc5um70ghs6atfm29pu3ww0 foreign key (site_id) references site;

alter table topo
    add constraint FKgflx285jx9gsc5et8uuca5nyr foreign key (user_id) references usr;

alter table user_role
    add constraint FKfpm8swft53ulq2hl11yplpr5 foreign key (user_id) references usr;

alter table voie
    add constraint FKqlo061c6fgkuosv9chywpgn8t foreign key (secteur_id) references secteur;
