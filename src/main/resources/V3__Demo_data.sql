INSERT INTO public.usr
VALUES (5, false, '2020-03-27 19:14:28.137', '2020-03-27 19:14:28.137', 'membre@membre.com', 'membre',
        '$2a$08$HZhlp3fy8ib22G5syv3GXeAdl0z18iRk00CvmgeFKr7WLrkdKI8ZC', 'membre');
INSERT INTO public.usr
VALUES (6, false, '2020-03-27 19:14:42.038', '2020-03-27 19:14:42.038', 'user@user.com', 'user',
        '$2a$08$57iNlYbmjkdupShNLwf08eSc/ahAxvhL/jZJSkW/k8wgJItnKuO/e', 'user');
INSERT INTO public.usr
VALUES (7, false, '2020-03-27 20:07:17.913', '2020-03-27 20:07:17.913', 'user2@user.com', 'user2',
        '$2a$08$jKU5f7ZxJsQp17aOJAcwoO082SYGl/il.tRHb8cp0dLMrMJpUi8D.', 'user2');

SELECT pg_catalog.setval('usr_id_seq', 7, true);


INSERT INTO public.user_role
VALUES (6, 'USER');
INSERT INTO public.user_role
VALUES (5, 'MEMBRE');
INSERT INTO public.user_role
VALUES (7, 'USER');


INSERT INTO public.site (id, city, code_post, cotation_max, date_creation, date_modified, description, name,
                         numbers_comments, numbers_secteurs, numbers_voies, official, region, street, createur_id)
VALUES (7, 'Soumy', 92300, null, '2020-03-27 20:13:07.000000', null,
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Siurana', 0, 0, 0, false, 'Nantes', '96 rue Aristide Briand 92300 LEVALLOIS PERRET', 6);
INSERT INTO public.site (id, city, code_post, cotation_max, date_creation, date_modified, description, name,
                         numbers_comments, numbers_secteurs, numbers_voies, official, region, street, createur_id)
VALUES (3, 'Soumy', 2727, '8B', '2020-03-27 20:08:45.884000', '2020-04-08 16:56:11.711000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Antalya', 0, 1, 1, true, 'Bretagne', '11 Boulevard Jules Guesde,', 7);
INSERT INTO public.site (id, city, code_post, cotation_max, date_creation, date_modified, description, name,
                         numbers_comments, numbers_secteurs, numbers_voies, official, region, street, createur_id)
VALUES (2, 'Soumy', 92300, '7C', '2020-03-27 20:13:04.000000', '2020-04-08 16:59:42.966000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Wadi Rum', 2, 5, 4, false, 'Kiev', '96 rue Aristide Briand 92300 LEVALLOIS PERRET', 6);
INSERT INTO public.site (id, city, code_post, cotation_max, date_creation, date_modified, description, name,
                         numbers_comments, numbers_secteurs, numbers_voies, official, region, street, createur_id)
VALUES (6, 'Soumy', 92300, '8A', '2020-03-27 20:13:06.000000', '2020-04-08 16:56:56.641000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Kalymnos', 0, 1, 1, false, 'Paris', '96 rue Aristide Briand 92300 LEVALLOIS PERRET', 6);
INSERT INTO public.site (id, city, code_post, cotation_max, date_creation, date_modified, description, name,
                         numbers_comments, numbers_secteurs, numbers_voies, official, region, street, createur_id)
VALUES (1, 'LEVALLOIS PERRET', 92300, '8C', '2020-03-27 20:12:49.000000', '2020-04-08 17:00:49.221000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Vallée de Chamonix', 0, 3, 2, false, 'Lion', '96 rue Aristide Briand 92300 LEVALLOIS PERRET', 6);
INSERT INTO public.site (id, city, code_post, cotation_max, date_creation, date_modified, description, name,
                         numbers_comments, numbers_secteurs, numbers_voies, official, region, street, createur_id)
VALUES (8, 'LEVALLOIS PERRET', 92300, '7B', '2020-03-27 19:44:12.873000', '2020-04-08 16:57:24.702000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'SURESNES ESCALADE', 1, 1, 1, false, 'Paris', '96 rue Aristide Briand 92300 LEVALLOIS PERRET', 6);
INSERT INTO public.site (id, city, code_post, cotation_max, date_creation, date_modified, description, name,
                         numbers_comments, numbers_secteurs, numbers_voies, official, region, street, createur_id)
VALUES (9, 'Soumy', 92300, '9A', '2020-03-27 20:13:08.000000', '2020-04-08 17:01:32.019000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Dignissimos', 0, 1, 1, false, 'Caen', '96 rue Aristide Briand 92300 LEVALLOIS PERRET', 7);
INSERT INTO public.site (id, city, code_post, cotation_max, date_creation, date_modified, description, name,
                         numbers_comments, numbers_secteurs, numbers_voies, official, region, street, createur_id)
VALUES (5, 'Soumy', 92300, '8A', '2020-03-27 20:13:06.000000', '2020-04-08 16:58:07.418000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Pont d’Espagne', 0, 4, 4, false, 'Harkiv', '96 rue Aristide Briand 92300 LEVALLOIS PERRET', 6);

SELECT pg_catalog.setval('site_id_seq', 9, true);



INSERT INTO public.comment
VALUES (1, '2020-03-27 20:44:49.455', '2020-03-27 20:44:49.455', 'Super!!', 5, 5);
INSERT INTO public.comment
VALUES (2, '2020-03-27 20:45:05.879', '2020-03-27 20:45:05.879', 'Super', 5, 5);
INSERT INTO public.comment
VALUES (3, '2020-03-27 20:45:17.388', '2020-03-27 20:45:17.388', 'Super', 5, 1);

SELECT pg_catalog.setval('comment_id_seq', 3, true);



INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (1, '2020-03-27 19:44:23.151000', '2020-03-27 19:44:23.151000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur', 1);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (19, '2020-04-08 16:57:56.116000', '2020-04-08 16:57:56.116000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur13', 5);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (7, '2020-03-27 20:23:00.181000', '2020-03-27 20:23:00.181000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur8', 5);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (2, '2020-03-27 19:55:22.754000', '2020-03-27 19:55:22.754000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur2', 1);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (5, '2020-03-27 20:09:30.143000', '2020-03-27 20:09:30.143000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur5', 2);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (15, '2020-04-08 16:46:10.962000', '2020-04-08 16:46:10.962000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur9', 2);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (16, '2020-04-08 16:52:25.001000', '2020-04-08 16:56:01.482000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur10', 3);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (20, '2020-04-08 16:59:25.584000', '2020-04-08 16:59:25.584000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur14', 2);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (8, '2020-03-27 20:23:06.845000', '2020-03-27 20:23:06.845000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur6', 5);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (22, '2020-04-08 17:01:23.552000', '2020-04-08 17:01:23.552000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur16', 9);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (17, '2020-04-08 16:56:47.626000', '2020-04-08 16:56:47.626000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur11', 6);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (21, '2020-04-08 17:00:38.377000', '2020-04-08 17:00:38.377000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur15', 1);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (4, '2020-03-27 20:09:12.530000', '2020-03-27 20:09:12.530000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur4', 2);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (3, '2020-03-27 20:08:59.117000', '2020-03-27 20:08:59.117000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur3', 2);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (18, '2020-04-08 16:57:16.861000', '2020-04-08 16:57:16.861000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur12', 8);
INSERT INTO public.secteur (id, date_creation, date_modified, description, name, site_id)
VALUES (6, '2020-03-27 20:22:51.591000', '2020-03-27 20:22:51.591000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Secteur7', 5);

SELECT pg_catalog.setval('secteur_id_seq', 22, true);



INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (12, '7B', '2020-04-08 16:57:24.688000', '2020-04-08 16:57:24.688000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Voie10', 18);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (15, '8C', '2020-04-08 17:00:49.214000', '2020-04-08 17:00:49.214000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Voie13', 1);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (2, '7C', '2020-03-27 20:09:51.214000', '2020-03-27 20:09:51.214000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        'Voie2', 3);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (14, '7B', '2020-04-08 16:59:42.959000', '2020-04-08 16:59:42.959000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        'Voie12', 4);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (6, '6B', '2020-03-27 20:23:48.595000', '2020-03-27 20:23:48.595000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        'Voie6', 7);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (10, '8B', '2020-04-08 16:56:11.699000', '2020-04-08 16:56:11.699000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Voie8', 16);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (16, '9A', '2020-04-08 17:01:32.010000', '2020-04-08 17:01:32.010000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Voie14', 22);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (13, '7C', '2020-04-08 16:58:07.397000', '2020-04-08 16:58:07.397000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Voie11', 7);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (8, '6A', '2020-03-27 20:31:00.781000', '2020-03-27 20:31:00.781000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        'Voie7', 6);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (5, '8A', '2020-03-27 20:23:18.073000', '2020-03-27 20:23:18.073000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Voie5', 6);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (4, '6A', '2020-03-27 20:11:09.710000', '2020-03-27 20:11:09.710000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        'Voie4', 4);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (3, '7A', '2020-03-27 20:10:04.550000', '2020-03-27 20:10:04.550000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        'Voie3', 3);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (11, '8A', '2020-04-08 16:56:56.605000', '2020-04-08 16:56:56.605000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Voie9', 17);
INSERT INTO public.voie (id, cotation, date_creation, date_modified, description, name, secteur_id)
VALUES (1, '6C', '2020-03-27 19:44:32.793000', '2020-03-27 19:44:32.793000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        'Voie', 1);

SELECT pg_catalog.setval('voie_id_seq', 16, true);



INSERT INTO public.longueur (id, date_creation, date_modified, description, height, name, voie_id)
VALUES (4, '2020-03-27 20:24:05.179000', '2020-03-27 20:24:05.179000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        85, 'Longueur4', 6);
INSERT INTO public.longueur (id, date_creation, date_modified, description, height, name, voie_id)
VALUES (3, '2020-03-27 20:23:36.136000', '2020-03-27 20:23:36.136000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        20, 'Longueur3', 5);
INSERT INTO public.longueur (id, date_creation, date_modified, description, height, name, voie_id)
VALUES (1, '2020-03-27 19:44:41.796000', '2020-03-27 19:44:41.796000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        100, 'Longueur', 1);
INSERT INTO public.longueur (id, date_creation, date_modified, description, height, name, voie_id)
VALUES (2, '2020-03-27 20:10:23.454000', '2020-03-27 20:10:23.454000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        200, 'Longueur2', 3);
INSERT INTO public.longueur (id, date_creation, date_modified, description, height, name, voie_id)
VALUES (8, '2020-04-08 16:59:35.369000', '2020-04-08 16:59:35.369000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        88, 'Longueur7', 3);
INSERT INTO public.longueur (id, date_creation, date_modified, description, height, name, voie_id)
VALUES (7, '2020-04-08 16:58:21.344000', '2020-04-08 16:58:21.344000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        44, 'Longueur6', 6);
INSERT INTO public.longueur (id, date_creation, date_modified, description, height, name, voie_id)
VALUES (6, '2020-04-08 16:56:22.414000', '2020-04-08 16:56:22.414000',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum modi neque repellat? A aliquid delectus distinctio doloremque eligendi et ex facilis iure, magnam mollitia nihil officia quae reprehenderit repudiandae.',
        77, 'Longueur5', 10);

SELECT pg_catalog.setval('longueur_id_seq', 8, true);



INSERT INTO public.topo (id, date_creation, date_modified, description, ispublic, name, reserved, site_id, user_id)
VALUES (2, '2020-03-27 20:31:18.163000', '2020-03-27 20:37:19.541000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        true, 'Topo', false, 1, 5);
INSERT INTO public.topo (id, date_creation, date_modified, description, ispublic, name, reserved, site_id, user_id)
VALUES (5, '2020-03-27 20:38:06.503000', '2020-03-27 20:39:25.611000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        true, 'Topo3', false, 6, 5);
INSERT INTO public.topo (id, date_creation, date_modified, description, ispublic, name, reserved, site_id, user_id)
VALUES (4, '2020-03-27 20:37:51.017000', '2020-03-27 20:39:28.311000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        true, 'Topo4', false, 2, 5);
INSERT INTO public.topo (id, date_creation, date_modified, description, ispublic, name, reserved, site_id, user_id)
VALUES (10, '2020-03-27 20:42:09.264000', '2020-03-27 20:42:10.843000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        true, 'Topo8', false, 1, 7);
INSERT INTO public.topo (id, date_creation, date_modified, description, ispublic, name, reserved, site_id, user_id)
VALUES (3, '2020-03-27 20:37:35.893000', '2020-03-27 20:42:44.157000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        true, 'Topo2', true, 5, 5);
INSERT INTO public.topo (id, date_creation, date_modified, description, ispublic, name, reserved, site_id, user_id)
VALUES (8, '2020-03-27 20:41:01.896000', '2020-04-08 16:49:08.217000',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos exercitationem alias esse iusto in illum autem, maiores consequatur quibusdam tempora eligendi enim eius eaque similique voluptas nostrum rerum dolores quia?',
        false, 'Topo6', false, 9, 6);

SELECT pg_catalog.setval('topo_id_seq', 10, true);



INSERT INTO public.reservation (id, annuled, confirmed, date_creation, date_modified, dismiss, topo_id, user_id)
VALUES (1, false, false, '2020-03-27 20:41:05.809000', '2020-03-27 20:41:05.809000', false, 2, 6);
INSERT INTO public.reservation (id, annuled, confirmed, date_creation, date_modified, dismiss, topo_id, user_id)
VALUES (3, false, false, '2020-03-27 20:41:10.130000', '2020-03-27 20:41:10.130000', false, 5, 6);
INSERT INTO public.reservation (id, annuled, confirmed, date_creation, date_modified, dismiss, topo_id, user_id)
VALUES (5, false, false, '2020-03-27 20:41:40.269000', '2020-03-27 20:41:40.269000', false, 4, 7);
INSERT INTO public.reservation (id, annuled, confirmed, date_creation, date_modified, dismiss, topo_id, user_id)
VALUES (6, false, true, '2020-03-27 20:41:43.074000', '2020-03-27 20:42:44.157000', false, 3, 7);
INSERT INTO public.reservation (id, annuled, confirmed, date_creation, date_modified, dismiss, topo_id, user_id)
VALUES (2, false, false, '2020-03-27 20:41:08.207000', '2020-03-27 20:41:08.207000', true, 3, 6);
INSERT INTO public.reservation (id, annuled, confirmed, date_creation, date_modified, dismiss, topo_id, user_id)
VALUES (4, true, false, '2020-03-27 20:41:13.055000', '2020-04-08 16:48:11.279000', false, 4, 6);

SELECT pg_catalog.setval('reservation_id_seq', 6, true);

















