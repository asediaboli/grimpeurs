package com.example.ocrp06.grimpeurs.repository.storage;


import com.example.ocrp06.grimpeurs.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Long> {

    User getOne(Long id);

    User findByNome(String nome);

    User findByEmail(String email);

}
