package com.example.ocrp06.grimpeurs.repository.storage;

import com.example.ocrp06.grimpeurs.entity.Reservation;
import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    Reservation getOne(Long id);

    List<Reservation> findAll();

    Page<Reservation> findByUser(User requesting, Pageable pageable);

    Page<Reservation> findByTopo_User(User owner, Pageable pageable);

    List<Reservation> findByTopoAndUser(Topo topo, User user);


    @Modifying
    @Transactional
    @Query(value = "DELETE from reservation where topo_id= ?1", nativeQuery = true)
    void deletTopos(Long topoId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE reservation SET dismiss = TRUE where topo_id= ?1 and confirmed = FALSE ", nativeQuery = true)
    void setDissmis(Long topoId);
}
