package com.example.ocrp06.grimpeurs.repository.storage;

import com.example.ocrp06.grimpeurs.entity.Site;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface SiteRepository extends JpaRepository<Site, Long>, QuerydslPredicateExecutor<Site> {

    Site getOne(Long id);

    Site findByName(String nome);

    Page<Site> findAll(Pageable pageable);

    List<Site> findAll();

    List<Site> findAll(Sort sort);

    @Query("SELECT region FROM Site ")
    List<String> allRegions();

}
