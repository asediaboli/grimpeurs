package com.example.ocrp06.grimpeurs.repository.storage;

import com.example.ocrp06.grimpeurs.entity.Comment;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    Comment getOne(Long id);

    List<Comment> findByAuthor(User author);

    List<Comment> findBySite(Site site);
}
