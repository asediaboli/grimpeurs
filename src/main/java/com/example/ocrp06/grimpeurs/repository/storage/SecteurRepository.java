package com.example.ocrp06.grimpeurs.repository.storage;

import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Site;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface SecteurRepository extends JpaRepository<Secteur, Long>, QuerydslPredicateExecutor<Secteur> {

    Secteur getOne(Long id);

    Secteur findByName(String name);

    Secteur findBySite(Site site);

    @Query(value = "SELECT max(count) from (SELECT count(site_id) from secteur group by site_id ) AS COUNT", nativeQuery = true)
    int maxSecteur();
}
