package com.example.ocrp06.grimpeurs.repository.storage;

import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TopoRepository extends JpaRepository<Topo, Long> {

    Topo getOne(Long id);

    Topo findByName(String name);

    Page<Topo> findByUser(User owner, Pageable pageable);

    List<Topo> findByIspublicTrue();

    Page<Topo> findByIspublicTrueAndReservedFalse(Pageable pageable);

}
