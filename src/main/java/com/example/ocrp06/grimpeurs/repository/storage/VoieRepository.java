package com.example.ocrp06.grimpeurs.repository.storage;

import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.entity.Voie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface VoieRepository extends JpaRepository<Voie, Long>, QuerydslPredicateExecutor<Site> {

    Voie getOne(Long id);

    Voie findByName(String name);

    Voie findBySecteur(Secteur secteur);

    @Query(value = "SELECT max(count) from (SELECT count(secteur_id) from voie group by secteur_id ) AS COUNT", nativeQuery = true)
    int maxVoie();
}
