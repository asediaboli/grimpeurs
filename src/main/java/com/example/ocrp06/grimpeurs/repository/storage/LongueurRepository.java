package com.example.ocrp06.grimpeurs.repository.storage;

import com.example.ocrp06.grimpeurs.entity.Longueur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LongueurRepository extends JpaRepository<Longueur, Long> {

    Longueur getOne(Long id);

    Longueur findByName(String name);
}
