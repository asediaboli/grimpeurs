package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Comment;
import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.service.CommentService;
import com.example.ocrp06.grimpeurs.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/user")
@PreAuthorize("hasAnyAuthority('MEMBRE', 'USER')")
public class CommentController {

    Authentication authentication;

    private final UserService userService;

    private final CommentService commentService;

    public CommentController(UserService userService, CommentService commentService) {
        this.userService = userService;
        this.commentService = commentService;
    }

    @PostMapping("/new/comment")
    public String saveComment(@ModelAttribute("newComment") @Valid Comment newComment,
                              BindingResult result,
                              @ModelAttribute("siteId") Site site,
                              Model model) {
        if (result.hasErrors()) {
            authentication = SecurityContextHolder.getContext().getAuthentication();
            List<Comment> comments = commentService.findBySite(site);
            model.addAttribute("site", site);
            model.addAttribute("comments", comments);
            model.addAttribute("user", authentication.getName());
            return "page/site-info";
        }
        authentication = SecurityContextHolder.getContext().getAuthentication();
        newComment.setSite(site);
        newComment.setAuthor(userService.findByEmail(authentication.getName()));
        commentService.saveNew(newComment);
        return "redirect:/site-info/" + site.getId();
    }

    @PostMapping("/comment/delete")
    public String deleteComment(@RequestParam("commentId") Comment comment,
                                Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(comment.getAuthor().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        commentService.delete(comment);
        return "redirect:/site-info/" + comment.getSite().getId();
    }

    @GetMapping("/comment/update")
    public String updateCommentPost(@ModelAttribute("commentId") Comment comment,
                                    Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(comment.getAuthor().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        model.addAttribute("commentUpdate", comment);
        return "page/comment-update";
    }

    @PostMapping("/comment/update")
    public String updateCommentGet(@ModelAttribute("commentId") Comment comment,
                                   @RequestParam("text") String newText,
                                   Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(comment.getAuthor().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        commentService.saveModify(comment);
        return "redirect:/site-info/" + comment.getSite().getId();
    }
}
