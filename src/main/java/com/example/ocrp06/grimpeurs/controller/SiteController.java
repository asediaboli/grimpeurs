package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.service.SiteService;
import com.example.ocrp06.grimpeurs.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@PreAuthorize("hasAnyAuthority('MEMBRE', 'USER')")
public class SiteController {

    private final SiteService siteService;

    private UserService userService;

    Authentication authentication;

    public SiteController(SiteService siteService, UserService userService) {
        this.siteService = siteService;
        this.userService = userService;
    }


    @GetMapping("/user/add-site")
    public String siteAddGet(@ModelAttribute("site") Site site,
                             Model model) {
        return "page/add-site";
    }

    @PostMapping("/user/add-site")
    public String siteAddPost(@ModelAttribute("site") @Valid Site site,
                              BindingResult result,
                              Model model) {
        if (result.hasErrors()) {
            return "page/add-site";
        }
        authentication = SecurityContextHolder.getContext().getAuthentication();
        site.setUser((userService.findByEmail(authentication.getName())));
        siteService.save(site);

        return "redirect:/user/site/modify?siteId=" + site.getId();
    }

    @GetMapping("/user/site/modify")
    public String siteModifyGet(@RequestParam("siteId") Site site,
                                Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(site.getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        model.addAttribute("site", site);
        return "page/site-modify";
    }

    @PostMapping("/user/site/modify")
    public String siteModifyPost(@ModelAttribute("site") Site siteModif,
                                 @RequestParam("siteId") Site site,
                                 Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(site.getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        siteService.save(siteService.modif(site, siteModif));

        return "redirect:/user/site/modify?siteId=" + site.getId();
    }

    @PostMapping("/user/site/delete")
    public String siteDel(@RequestParam("siteId") Site site,
                          Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(site.getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        siteService.delete(site);
        return "redirect:/sites";
    }


}
