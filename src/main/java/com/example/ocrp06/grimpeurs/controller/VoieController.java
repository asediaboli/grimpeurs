package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Voie;
import com.example.ocrp06.grimpeurs.service.SecteurService;
import com.example.ocrp06.grimpeurs.service.VoieService;
import com.example.ocrp06.grimpeurs.utils.Utils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@PreAuthorize("hasAnyAuthority('MEMBRE', 'USER')")
public class VoieController {

    Authentication authentication;

    private final VoieService voieService;

    private final SecteurService secteurService;

    public VoieController(VoieService voieService, SecteurService secteurService) {
        this.voieService = voieService;
        this.secteurService = secteurService;
    }

    @GetMapping("/user/add-voie")
    public String addVoieGet(@RequestParam("secteurId") Secteur secteur,
                             @ModelAttribute("voie") Voie voie,
                             Model model) {
        model.addAttribute("secteur", secteur);
        model.addAttribute("listCotations", Utils.cotationArrayList());
        return "page/add-voie";
    }

    @PostMapping("/user/add-voie")
    public String addVoiePost(@ModelAttribute("voie") @Valid Voie voie,
                              BindingResult result,
                              @RequestParam("secteurId") Secteur secteur,
                              Model model) {
        if (result.hasErrors()) {
            model.addAttribute("secteur", secteur);
            model.addAttribute("listCotations", Utils.cotationArrayList());
            return "page/add-voie";
        }
        voie.setSecteur(secteur);
        voieService.save(voie);
        return "redirect:/user/site/modify?siteId=" + secteur.getSite().getId();
    }

    @GetMapping("/user/voie/modify")
    public String voieModifGet(@RequestParam("voieId") Voie voie,
                               Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(voie.getSecteur().getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        model.addAttribute("voie", voie);
        model.addAttribute("listCotations", Utils.cotationArrayList());
        return "page/modify-voie";
    }

    @PostMapping("/user/voie/modify")
    public String voieModifyPost(@ModelAttribute("voie") Voie voieModif,
                                 @RequestParam("voieId") Voie voie,
                                 Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(voie.getSecteur().getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        voieService.save(voieService.modify(voie, voieModif));
        return "redirect:/user/site/modify?siteId=" + voie.getSecteur().getSite().getId();
    }

    @PostMapping("/user/voie/delete")
    public String voieDel(@RequestParam("voieId") Voie voie,
                          @RequestParam("siteId") Long siteId,
                          Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(voie.getSecteur().getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        voieService.delete(voie);
        return "redirect:/user/site/modify?siteId=" + siteId;
    }
}
