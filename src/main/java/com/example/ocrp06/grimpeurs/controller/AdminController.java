package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.User;
import com.example.ocrp06.grimpeurs.service.AdminService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin")
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {

    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping
    public String userList(Model model) {
        model.addAttribute("roles", adminService.rolesToSet());
        model.addAttribute("users", adminService.findAll());

        return "page/admin-users-list";
    }

    @PostMapping
    public String saveRole(@RequestParam("userId") User user,
                           @RequestParam("userRole") String newRole,
                           Model model) {
        user.getRoles().clear();
        user.getRoles().add(Role.valueOf(newRole));
        adminService.save(user);
        return "redirect:/admin";
    }
}
