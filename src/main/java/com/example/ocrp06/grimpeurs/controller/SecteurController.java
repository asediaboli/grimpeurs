package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.service.SecteurService;
import com.example.ocrp06.grimpeurs.service.SiteService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@PreAuthorize("hasAnyAuthority('MEMBRE', 'USER')")
public class SecteurController {

    Authentication authentication;
    private final SiteService siteService;

    private final SecteurService secteurService;

    public SecteurController(SiteService siteService, SecteurService secteurService) {
        this.siteService = siteService;
        this.secteurService = secteurService;
    }

    @GetMapping("/user/add-secteur")
    public String addSecteurGet(@ModelAttribute("secteur") Secteur secteur,
                                @RequestParam("siteId") Site site,
                                Model model) {
        model.addAttribute("site", site);
        return "page/add-secteur";
    }

    @PostMapping("/user/add-secteur")
    public String addSecteurPost(@ModelAttribute("secteur") @Valid Secteur secteur,
                                 BindingResult result,
                                 @RequestParam("siteId") Site site,
                                 Model model) {
        if (result.hasErrors()) {
            model.addAttribute("site", site);
            return "page/add-secteur";
        }
        secteur.setSite(site);
        secteurService.save(secteur);
        return "redirect:/user/site/modify?siteId=" + site.getId();
    }

    @PostMapping("/user/secteur/delete")
    public String secteurDel(@RequestParam("secteurId") Secteur secteur,
                             @RequestParam("siteId") Long siteId,
                             Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(secteur.getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        secteurService.delete(secteur);
        return "redirect:/user/site/modify?siteId=" + siteId;
    }

    @GetMapping("/user/secteur/modify")
    public String secteurModifGet(@RequestParam("secteurId") Secteur secteur,
                                  Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(secteur.getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        model.addAttribute("listSites", siteService.findAll());
        model.addAttribute("secteur", secteur);
        return "page/modify-secteur";
    }

    @PostMapping("/user/secteur/modify")
    public String secteurModifyPost(@ModelAttribute("secteur") Secteur secteurModif,
                                    @RequestParam("secteurIdModif") Secteur secteur,
                                    Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(secteur.getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        secteurService.save(secteurService.modify(secteur, secteurModif));
        return "redirect:/user/site/modify?siteId=" + secteur.getSite().getId();
    }
}
