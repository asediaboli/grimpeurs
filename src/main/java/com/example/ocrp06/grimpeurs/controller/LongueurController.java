package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Longueur;
import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.Voie;
import com.example.ocrp06.grimpeurs.service.LongueurService;
import com.example.ocrp06.grimpeurs.service.VoieService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@PreAuthorize("hasAnyAuthority('MEMBRE', 'USER')")
public class LongueurController {

    Authentication authentication;

    private final VoieService voieService;

    private final LongueurService longueurService;

    public LongueurController(VoieService voieService, LongueurService longueurService) {
        this.voieService = voieService;
        this.longueurService = longueurService;
    }

    @GetMapping("/user/add-longueur")
    public String addVoieGet(@ModelAttribute("longueur") Longueur longueur,
                             @RequestParam("voieId") Voie voie,
                             Model model) {
        model.addAttribute("voie", voie);
        return "page/add-longueur";
    }

    @PostMapping("/user/add-longueur")
    public String addVoiePost(@ModelAttribute("longueur") @Valid Longueur longueur,
                              BindingResult result,
                              @RequestParam("voieId") Voie voie,
                              Model model) {
        if (result.hasErrors()) {
            model.addAttribute("voie", voie);
            return "page/add-longueur";
        }
        longueur.setVoie(voie);
        longueurService.save(longueur);
        return "redirect:/user/site/modify?siteId=" + longueur.getVoie().getSecteur().getSite().getId();
    }

    @GetMapping("/user/longueur/modify")
    public String longueurModifGet(@RequestParam("longueurId") Longueur longueur,
                                   Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(longueur.getVoie().getSecteur().getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        model.addAttribute("longueur", longueur);
        return "page/modify-longueur";
    }

    @PostMapping("/user/longueur/modify")
    public String longueurModifyPost(@ModelAttribute("longueur") Longueur longueurModif,
                                     @RequestParam("longueurId") Longueur longueur,
                                     Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(longueur.getVoie().getSecteur().getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        longueurService.save(longueurService.modify(longueur, longueurModif));
        return "redirect:/user/site/modify?siteId=" + longueur.getVoie().getSecteur().getSite().getId();
    }

    @PostMapping("/user/longueur/delete")
    public String longueurDel(@RequestParam("longueurId") Longueur longueur,
                              @RequestParam("siteId") Long siteId,
                              Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(longueur.getVoie().getSecteur().getSite().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        longueurService.delete(longueur);
        return "redirect:/user/site/modify?siteId=" + siteId;
    }
}
