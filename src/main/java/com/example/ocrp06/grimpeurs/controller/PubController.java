package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Comment;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.service.CommentService;
import com.example.ocrp06.grimpeurs.service.SiteService;
import com.example.ocrp06.grimpeurs.service.TopoService;
import com.example.ocrp06.grimpeurs.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.IntStream;

@Controller
public class PubController {

    private final SiteService siteService;
    private CommentService commentService;
    private TopoService topoService;
    Authentication authentication;

    public PubController(SiteService siteService, CommentService commentService, TopoService topoService) {
        this.siteService = siteService;
        this.commentService = commentService;
        this.topoService = topoService;
    }

    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }


    @GetMapping("/sites")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "") String nameSite,
                           @RequestParam(name = "selectRegion", required = false, defaultValue = "") String selectRegion,
                           @RequestParam(name = "cotation", required = false, defaultValue = "") String cotation,
                           @RequestParam(name = "isOfficiel", required = false, defaultValue = "FALSE") String isOfficiel,
                           @RequestParam(name = "nombreSecteurs", required = false, defaultValue = "0") String nombreSecteurs,
                           @RequestParam(name = "nombreVoies", required = false, defaultValue = "0") String nombreVoies,
                           Model model,
                           @PageableDefault(sort = {"official"}, direction = Sort.Direction.DESC, size = 6) Pageable pageable) {

        Page<Site> page = siteService.filter(pageable, selectRegion, cotation, isOfficiel, nombreSecteurs, nombreVoies, nameSite);
        model.addAttribute("escalades", page);
        model.addAttribute("allRegions", siteService.allRegions());
        model.addAttribute("listCotations", Utils.cotationArrayList());
        model.addAttribute("url", "/sites?selectRegion=" + selectRegion + "&cotation=" + cotation + "&isOfficiel=" + isOfficiel + "&nombreSecteurs" + nombreSecteurs + "&nombreVoies=" + nombreVoies + "&name=" + nameSite);
        model.addAttribute("numberOfPages", IntStream.rangeClosed(1, page.getTotalPages()).toArray());
        return "page/sites";
    }

    @PostMapping("/sites")
    public String filter(final Site site, Model model) {

        return "page/sites";
    }

    @GetMapping("/site-info/{siteId}")
    public String siteInfo(@ModelAttribute("newComment") Comment newComment,
                           @PathVariable("siteId") Site site,
                           Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        List<Comment> comments = commentService.findBySite(site);
        model.addAttribute("site", site);
        model.addAttribute("comments", comments);
        model.addAttribute("user", authentication.getName());
        return "page/site-info";
    }

    @GetMapping("/topos")
    public String topos(Model model,
                        @PageableDefault(sort = "name", direction = Sort.Direction.ASC, size = 6) Pageable pageable) {
        Page<Topo> topos = topoService.findByPublicTrueAndReservedFalse(pageable);
        model.addAttribute("topos", topos);
        model.addAttribute("url", "/topos");
        model.addAttribute("numberOfPages", IntStream.rangeClosed(1, topos.getTotalPages()).toArray());
        return "page/topos";
    }


}
