package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.service.LongueurService;
import com.example.ocrp06.grimpeurs.service.SecteurService;
import com.example.ocrp06.grimpeurs.service.SiteService;
import com.example.ocrp06.grimpeurs.service.VoieService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/membre")
@PreAuthorize("hasAuthority('MEMBRE')")
public class MembreController {

    private final SiteService siteService;
    private final SecteurService secteurService;
    private final VoieService voieService;
    private final LongueurService longueurService;

    public MembreController(SiteService siteService, SecteurService secteurService, VoieService voieService, LongueurService longueurService) {
        this.siteService = siteService;
        this.secteurService = secteurService;
        this.voieService = voieService;
        this.longueurService = longueurService;
    }


    @PostMapping("/tags")
    public String faireTag(@RequestParam("siteId") Site site,
                           Model model) {
        if (site.isOfficial()) {
            site.setOfficial(Boolean.FALSE);
        } else {
            site.setOfficial(Boolean.TRUE);
        }
        siteService.save(site);
        return "redirect:/sites";
    }















}
