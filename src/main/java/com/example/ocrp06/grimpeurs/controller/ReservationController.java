package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Reservation;
import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.service.ReservationService;
import com.example.ocrp06.grimpeurs.service.TopoService;
import com.example.ocrp06.grimpeurs.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.stream.IntStream;

@Controller
@RequestMapping("/user")
@PreAuthorize("hasAnyAuthority('MEMBRE', 'USER')")
public class ReservationController {

    private final ReservationService reservationService;

    private final TopoService topoService;

    private final UserService userService;

    Authentication authentication;

    public ReservationController(ReservationService reservationService, TopoService topoService, UserService userService) {
        this.reservationService = reservationService;
        this.topoService = topoService;
        this.userService = userService;
    }

    @PostMapping("/reservations")
    public String topoReservation(@RequestParam("reservTopoId") Topo topo,
                                  Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Reservation reservation = new Reservation();
        reservation.setUser(userService.findByEmail(authentication.getName()));
        reservation.setTopo(topo);

        if (reservationService.isExist(reservation.getTopo(), userService.findByEmail(authentication.getName()))) {
            return "redirect:/user/reservations?exist";
        }
        reservationService.save(reservation);
        return "redirect:/user/reservations";
    }


    @GetMapping("/reservations")
    public String reservation(Model model,
                              @Qualifier("top") @PageableDefault(sort = "dateCreation", direction = Sort.Direction.DESC, size = 5) Pageable pageable,
                              @Qualifier("bottom") @PageableDefault(sort = "dateCreation", direction = Sort.Direction.DESC, size = 5) Pageable pageable2) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Page<Reservation> reservations = reservationService.findByUserRequesting(userService.findByEmail(authentication.getName()), pageable);
        Page<Reservation> reservationTopoOwner = reservationService.findByUserOwnerTopo(userService.findByEmail(authentication.getName()), pageable2);
        model.addAttribute("reservations", reservations);
        model.addAttribute("reservationTopoOwner", reservationTopoOwner);
        model.addAttribute("pagesTop", IntStream.rangeClosed(1, reservations.getTotalPages()).toArray());
        model.addAttribute("pagesBottom", IntStream.rangeClosed(1, reservationTopoOwner.getTotalPages()).toArray());
        model.addAttribute("url", "/user/reservations");
        return "page/reservation";
    }

    @PostMapping("/reservation/confirm")
    public String reservationConfirm(@RequestParam("reservationConf") Reservation reservation, Model model) {
        reservation.setConfirmed(Boolean.TRUE);
        reservation.getTopo().setReserved(Boolean.TRUE);
        reservationService.setDismiss(reservation.getTopo());
        reservationService.save(reservation);
        return "redirect:/user/reservations";
    }

    @PostMapping("/reservation/delete")
    public String reservationDel(@RequestParam("reservationDelete") Reservation reservation, Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(reservation.getUser().getEmail()) & !authentication.getName().equalsIgnoreCase(reservation.getTopo().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        if (reservation.isConfirmed()) {
            reservation.getTopo().setReserved(Boolean.FALSE);
        }

        reservationService.delete(reservation);
        return "redirect:/user/reservations";
    }

    @PostMapping("/reservationOwner/delete")
    public String reservationDelByOwner(@RequestParam("reservationDelete") Reservation reservation, Model model) {
        if (!authentication.getName().equalsIgnoreCase(reservation.getUser().getEmail()) & !authentication.getName().equalsIgnoreCase(reservation.getTopo().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        if (reservation.isConfirmed()) {
            Topo topo = reservation.getTopo();
            topo.setReserved(Boolean.FALSE);
            topoService.save(topo);
        }
        reservation.setConfirmed(Boolean.FALSE);
        reservation.setAnnuled(Boolean.TRUE);
        reservationService.save(reservation);
        return "redirect:/user/reservations";
    }

    @PostMapping("/reservation/dismiss")
    public String reservationDismiss(@RequestParam("reservationDis") Reservation reservation, Model model) {
        if (!authentication.getName().equalsIgnoreCase(reservation.getUser().getEmail()) & !authentication.getName().equalsIgnoreCase(reservation.getTopo().getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        reservation.setDismiss(Boolean.TRUE);
        reservationService.save(reservation);
        return "redirect:/user/reservations";
    }

}
