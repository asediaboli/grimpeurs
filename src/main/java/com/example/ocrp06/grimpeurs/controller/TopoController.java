package com.example.ocrp06.grimpeurs.controller;

import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.service.TopoService;
import com.example.ocrp06.grimpeurs.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.IntStream;

@Controller
@PreAuthorize("hasAnyAuthority('MEMBRE', 'USER')")
public class TopoController {

    Authentication authentication;

    private final TopoService topoService;

    private final UserService userService;

    public TopoController(TopoService topoService, UserService userService) {
        this.topoService = topoService;
        this.userService = userService;
    }


    @GetMapping("user/add-topo/{siteId}")
    public String topoGet(@PathVariable("siteId") Site site,
                          @ModelAttribute("topo") Topo topo,
                          Model model) {
        model.addAttribute("site", site);
        return "page/add-topo";
    }

    @PostMapping("user/add-topo/{siteId}")
    public String topoSave(@PathVariable("siteId") Site site,
                           @ModelAttribute("topo") @Valid Topo topo,
                           BindingResult result,
                           Model model) {
        if (result.hasErrors()) {
            model.addAttribute("site", site);
            return "page/add-topo";
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        topo.setSite(site);
        topo.setUser(userService.findByEmail(authentication.getName()));
        topoService.save(topo);

        return "redirect:/user/topos";
    }


    @PostMapping("/user/topos/delete")
    public String topoDel(@RequestParam("topoIdDel") Topo topo,
                          Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(topo.getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        topoService.delete(topo);
        return "redirect:/topos";
    }

    @GetMapping("/user/topos/modify")
    public String topoModifGet(@RequestParam("topoIdModif") Topo topo,
                               Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(topo.getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        model.addAttribute("topo", topo);
        return "page/modify-topo";
    }

    @PostMapping("/user/topos/modify")
    public String topoModif(@ModelAttribute("topo") @Valid Topo topo,
                            @RequestParam("topoIdModif") Topo topoModif,
                            Model model) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getName().equalsIgnoreCase(topoModif.getUser().getEmail()) & !authentication.getAuthorities().contains(Role.MEMBRE)) {
            return "error";
        }
        topoService.save(topoService.modify(topoModif, topo));
        return "redirect:/user/topos";
    }

    @GetMapping("/user/topos")
    public String userTopos(@RequestParam(name = "topoIdSetPub", required = false) Topo topo,
                            @PageableDefault(sort = "name", direction = Sort.Direction.ASC, size = 6) Pageable pageable,
                            Model model) {
        if (topo != null) {
            topo.setPublic(Boolean.TRUE);

        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Page<Topo> topos = topoService.findByUser(userService.findByEmail(authentication.getName()), pageable);
        model.addAttribute("topos", topos);
        model.addAttribute("url", "/user/topos");
        model.addAttribute("numberOfPages", IntStream.rangeClosed(1, topos.getTotalPages()).toArray());
        return "page/topos-user";
    }

    @PostMapping("/user/topos")
    public String setToposPub(@RequestParam("topoIdSetPub") Topo topo,
                              Model model) {
        if (topo.isPublic() == Boolean.TRUE) {
            topo.setPublic(Boolean.FALSE);
        } else {
            topo.setPublic(Boolean.TRUE);
        }

        topoService.save(topo);
        return "redirect:/user/topos";
    }
}
