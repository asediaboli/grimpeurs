package com.example.ocrp06.grimpeurs.service.impl;

import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.entity.User;
import com.example.ocrp06.grimpeurs.repository.storage.TopoRepository;
import com.example.ocrp06.grimpeurs.service.TopoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("TopoService")
public class TopoServiceImpl implements TopoService {

    private final TopoRepository topoRepository;

    public TopoServiceImpl(TopoRepository topoRepository) {
        this.topoRepository = topoRepository;
    }

    @Override
    public Topo getOne(Long id) {
        return topoRepository.getOne(id);
    }

    @Override
    public Topo findByName(String name) {
        return topoRepository.findByName(name);
    }

    @Override
    public Page<Topo> findByUser(User owner, Pageable pageable) {
        return topoRepository.findByUser(owner, pageable);
    }

    @Override
    public List<Topo> findAll() {
        return topoRepository.findAll();
    }

    @Override
    public Topo save(Topo topo) {
        return topoRepository.save(topo);
    }

    @Override
    public List<Topo> findByPublicTrue() {
        return topoRepository.findByIspublicTrue();
    }

    @Override
    public Page<Topo> findByPublicTrueAndReservedFalse(Pageable pageable) {
        return topoRepository.findByIspublicTrueAndReservedFalse(pageable);
    }

    @Override
    public void delete(Topo topo) {
        topoRepository.delete(topo);
    }

    @Override
    public Topo modify(Topo topoModify, Topo newTopo) {
        if (!newTopo.getName().isEmpty() & !newTopo.getName().equalsIgnoreCase(topoModify.getName())) {
            topoModify.setName(newTopo.getName());
        }
        if (!newTopo.getDescription().isEmpty() & !newTopo.getDescription().equalsIgnoreCase(topoModify.getDescription())) {
            topoModify.setDescription(newTopo.getDescription());
        }
        return topoModify;
    }
}
