package com.example.ocrp06.grimpeurs.service;


import com.example.ocrp06.grimpeurs.DTO.UserRegistrationDto;
import com.example.ocrp06.grimpeurs.entity.User;


public interface UserService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
}
