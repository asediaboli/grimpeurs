package com.example.ocrp06.grimpeurs.service.impl;

import com.example.ocrp06.grimpeurs.DTO.UserRegistrationDto;
import com.example.ocrp06.grimpeurs.entity.Role;
import com.example.ocrp06.grimpeurs.entity.User;
import com.example.ocrp06.grimpeurs.repository.storage.UserRepository;
import com.example.ocrp06.grimpeurs.service.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service(value = "UserService")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User save(UserRegistrationDto registration) {
        User user = new User();
        user.setNome(registration.getNome());
        user.setPrenom(registration.getPrenom());
        user.setEmail(registration.getEmail());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));
        user.setRoles(Collections.singleton(Role.USER));
        return userRepository.save(user);
    }
}
