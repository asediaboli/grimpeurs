package com.example.ocrp06.grimpeurs.service;

import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TopoService {

    Topo getOne(Long id);

    Topo findByName(String name);

    Page<Topo> findByUser(User owner, Pageable pageable);

    Topo save(Topo topo);

    List<Topo> findAll();

    List<Topo> findByPublicTrue();

    Page<Topo> findByPublicTrueAndReservedFalse(Pageable pageable);

    void delete(Topo topo);

    Topo modify(Topo topoModify, Topo newTopo);
}
