package com.example.ocrp06.grimpeurs.service.impl;

import com.example.ocrp06.grimpeurs.entity.Comment;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.entity.User;
import com.example.ocrp06.grimpeurs.repository.storage.CommentRepository;
import com.example.ocrp06.grimpeurs.service.CommentService;
import com.example.ocrp06.grimpeurs.service.SiteService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CommentService")
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    private SiteService siteService;

    public CommentServiceImpl(CommentRepository commentRepository, SiteService siteService) {
        this.siteService = siteService;
        this.commentRepository = commentRepository;
    }

    @Override
    public Comment getOne(Long id) {
        return commentRepository.getOne(id);
    }

    @Override
    public List<Comment> findByAuthor(User author) {
        return commentRepository.findByAuthor(author);
    }

    @Override
    public List<Comment> findBySite(Site site) {
        return commentRepository.findBySite(site);
    }


    @Override
    public Comment saveNew(Comment comment) {
        comment.getSite().setNumbersComments(comment.getSite().getNumbersComments() + 1);
        commentRepository.save(comment);
        return comment;
    }

    @Override
    public Comment saveModify(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public void delete(Comment comment) {
        comment.getSite().setNumbersComments(comment.getSite().getNumbersComments() - 1);
        commentRepository.delete(comment);
    }
}
