package com.example.ocrp06.grimpeurs.service;

import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Voie;

import java.util.List;

public interface VoieService {

    Voie save(Voie voie);

    Voie findByName(String name);

    Voie findByParent(Secteur secteur);

    List<Voie> findAll();

    void delete(Voie voie);

    Voie modify(Voie oldVoie, Voie newVoie);

}
