package com.example.ocrp06.grimpeurs.service;

import com.example.ocrp06.grimpeurs.entity.Longueur;

public interface LongueurService {

    Longueur save(Longueur longueur);

    void delete(Longueur longueur);

    Longueur modify(Longueur oldLongueur, Longueur newLongueur);

}
