package com.example.ocrp06.grimpeurs.service.impl;

import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Voie;
import com.example.ocrp06.grimpeurs.repository.storage.VoieRepository;
import com.example.ocrp06.grimpeurs.service.SiteService;
import com.example.ocrp06.grimpeurs.service.VoieService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("VoieService")
public class VoieServiceImpl implements VoieService {

    private final VoieRepository voieRepository;

    private final SiteService siteService;

    public VoieServiceImpl(VoieRepository voieRepository, SiteService siteService) {
        this.voieRepository = voieRepository;
        this.siteService = siteService;
    }

    @Override
    public Voie save(Voie voie) {
        voieRepository.save(voie);
        siteService.setCotationMax(voie.getSecteur().getSite());
        siteService.setNumbersSecteurAndVoie(voie.getSecteur().getSite());
        return voie;
    }

    @Override
    public Voie findByName(String name) {
        return voieRepository.findByName(name);
    }

    @Override
    public Voie findByParent(Secteur secteur) {
        return voieRepository.findBySecteur(secteur);
    }


    @Override
    public List<Voie> findAll() {
        return voieRepository.findAll();
    }

    @Override
    public void delete(Voie voie) {
        voieRepository.delete(voie);
        siteService.setCotationMax(voie.getSecteur().getSite());
        siteService.setNumbersSecteurAndVoie(voie.getSecteur().getSite());
    }

    @Override
    public Voie modify(Voie oldVoie, Voie newVoie) {
        if (!newVoie.getName().isEmpty() & !newVoie.getName().equalsIgnoreCase(oldVoie.getName())) {
            oldVoie.setName(newVoie.getName());
        }
        if (!newVoie.getCotation().isEmpty() & !newVoie.getCotation().equalsIgnoreCase(oldVoie.getCotation())) {
            oldVoie.setCotation(newVoie.getCotation());
        }
        if (!newVoie.getDescription().isEmpty() & !newVoie.getDescription().equalsIgnoreCase(oldVoie.getCotation())) {
            oldVoie.setDescription(newVoie.getDescription());
        }
        return oldVoie;
    }
}
