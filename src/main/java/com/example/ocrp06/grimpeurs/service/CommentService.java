package com.example.ocrp06.grimpeurs.service;

import com.example.ocrp06.grimpeurs.entity.Comment;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.entity.User;

import java.util.List;

public interface CommentService {

    Comment getOne(Long id);

    List<Comment> findByAuthor(User author);

    List<Comment> findBySite(Site site);


    void delete(Comment comment);

    Comment saveNew(Comment comment);

    Comment saveModify(Comment comment);
}
