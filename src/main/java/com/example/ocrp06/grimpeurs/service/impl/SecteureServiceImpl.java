package com.example.ocrp06.grimpeurs.service.impl;

import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.repository.storage.SecteurRepository;
import com.example.ocrp06.grimpeurs.service.SecteurService;
import com.example.ocrp06.grimpeurs.service.SiteService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("SecteurService")
public class SecteureServiceImpl implements SecteurService {

    private final SecteurRepository secteurRepository;
    private SiteService siteService;

    public SecteureServiceImpl(SecteurRepository secteurRepository, SiteService siteService) {
        this.secteurRepository = secteurRepository;
        this.siteService = siteService;
    }


    public Secteur save(Secteur secteur) {
        secteurRepository.save(secteur);
        siteService.setNumbersSecteurAndVoie(secteur.getSite());
        return secteur;
    }

    public Secteur findByName(String name) {
        return secteurRepository.findByName(name);
    }

    public Secteur findByParent(Site site) {
        return secteurRepository.findBySite(site);
    }

    @Override
    public List<Secteur> findAll() {
        return secteurRepository.findAll();
    }

    @Override
    public void delete(Secteur secteur) {
        secteurRepository.delete(secteur);
        siteService.setNumbersSecteurAndVoie(secteur.getSite());
    }

    @Override
    public Secteur modify(Secteur oldSecteur, Secteur newSecteur) {
        if (!newSecteur.getName().isEmpty() & !newSecteur.getName().equalsIgnoreCase(oldSecteur.getName())) {
            oldSecteur.setName(newSecteur.getName());
        }
        if (!newSecteur.getDescription().isEmpty() & !newSecteur.getDescription().equalsIgnoreCase(oldSecteur.getDescription())) {
            oldSecteur.setDescription(newSecteur.getDescription());
        }
        return oldSecteur;
    }
}
