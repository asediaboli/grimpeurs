package com.example.ocrp06.grimpeurs.service.impl;

import com.example.ocrp06.grimpeurs.entity.Longueur;
import com.example.ocrp06.grimpeurs.repository.storage.LongueurRepository;
import com.example.ocrp06.grimpeurs.service.LongueurService;
import org.springframework.stereotype.Service;

@Service("LongueurService")
public class LongueurServiceImpl implements LongueurService {

    private final LongueurRepository longueurRepository;

    public LongueurServiceImpl(LongueurRepository longueurRepository) {
        this.longueurRepository = longueurRepository;
    }

    @Override
    public Longueur save(Longueur longueur) {
        return longueurRepository.save(longueur);
    }

    @Override
    public void delete(Longueur longueur) {
        longueurRepository.delete(longueur);
    }

    @Override
    public Longueur modify(Longueur oldLongueur, Longueur newLongueur) {
        if (!newLongueur.getName().isEmpty() & !newLongueur.getName().equalsIgnoreCase(oldLongueur.getName())) {
            oldLongueur.setName(newLongueur.getName());
        }
        if (newLongueur.getHeight() != oldLongueur.getHeight()) {
            oldLongueur.setHeight(newLongueur.getHeight());
        }
        if (!newLongueur.getDescription().isEmpty() & !newLongueur.getDescription().equalsIgnoreCase(oldLongueur.getDescription())) {
            oldLongueur.setDescription(newLongueur.getDescription());
        }
        return oldLongueur;
    }
}
