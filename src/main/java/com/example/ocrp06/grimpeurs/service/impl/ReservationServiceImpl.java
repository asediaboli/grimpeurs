package com.example.ocrp06.grimpeurs.service.impl;

import com.example.ocrp06.grimpeurs.entity.Reservation;
import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.entity.User;
import com.example.ocrp06.grimpeurs.repository.storage.ReservationRepository;
import com.example.ocrp06.grimpeurs.service.ReservationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ReervationService")
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    public ReservationServiceImpl(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Reservation getOne(Long id) {
        return reservationRepository.getOne(id);
    }

    @Override
    public List<Reservation> findAll() {
        return reservationRepository.findAll();
    }

    @Override
    public Page<Reservation> findByUserRequesting(User requesting, Pageable pageable) {
        return reservationRepository.findByUser(requesting, pageable);
    }

    @Override
    public Page<Reservation> findByUserOwnerTopo(User owner, Pageable pageable) {
        return reservationRepository.findByTopo_User(owner, pageable);
    }

    @Override
    public Reservation save(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    @Override
    public void setDismiss(Topo topo) {
        reservationRepository.setDissmis(topo.getId());
    }

    @Override
    public void deleteByTopo(Topo topo) {
        reservationRepository.deletTopos(topo.getId());
    }

    @Override
    public void delete(Reservation reservationID) {
        reservationRepository.delete(reservationID);
    }

    @Override
    public List<Reservation> findByTopoAndUser(Topo topo, User user) {
        return reservationRepository.findByTopoAndUser(topo, user);
    }

    @Override
    //лист резерваций
    public boolean isExist(Topo topo, User user) {
        List<Reservation> reservation = reservationRepository.findByTopoAndUser(topo, user);
        for (Reservation res : reservation) {
            if (!res.isDismiss() && !res.isAnnuled() && !res.isConfirmed()) {
                return true;
            }
        }
        return false;
    }
}
