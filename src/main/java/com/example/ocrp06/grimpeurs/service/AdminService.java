package com.example.ocrp06.grimpeurs.service;

import com.example.ocrp06.grimpeurs.entity.User;

import java.util.List;
import java.util.Set;

public interface AdminService {

    User save(User user);

    List<User> findAll();

    User getOne(Long id);

    Set<String> rolesToSet();
}
