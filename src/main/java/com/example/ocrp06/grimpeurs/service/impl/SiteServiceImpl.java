package com.example.ocrp06.grimpeurs.service.impl;

import com.example.ocrp06.grimpeurs.entity.QSite;
import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Site;
import com.example.ocrp06.grimpeurs.entity.Voie;
import com.example.ocrp06.grimpeurs.repository.storage.SiteRepository;
import com.example.ocrp06.grimpeurs.service.SiteService;
import com.example.ocrp06.grimpeurs.utils.Utils;
import com.querydsl.core.BooleanBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("SiteService")
public class SiteServiceImpl implements SiteService {

    private final SiteRepository siteRepository;

    public SiteServiceImpl(SiteRepository siteRepository) {
        this.siteRepository = siteRepository;
    }

    public Site save(Site site) {
        return siteRepository.save(site);
    }

    @Override
    public void delete(Site site) {
        siteRepository.delete(site);
    }

    public Site findByName(String name) {
        return siteRepository.findByName(name);
    }

    @Override
    public Page<Site> findAll(Pageable pageable) {
        return siteRepository.findAll(pageable);
    }

    @Override
    public List<Site> findAll() {
        return siteRepository.findAll();
    }

    @Override
    public List<Site> findAll(Sort sort) {
        return siteRepository.findAll(sort);
    }

    @Override
    public Site getOne(Long siteId) {
        return siteRepository.getOne(siteId);
    }

    @Override
    public Set<String> allRegions() {
        Set<String> myset = new HashSet<>(siteRepository.allRegions());
        return myset;
    }

    public void setCotationMax(Site site) {
        ArrayList<Integer> cotations = new ArrayList<>();
        ArrayList<String> cotationArrayList = Utils.cotationArrayList();
        for (Secteur secteur : site.getSecteursList()) {
            for (Voie voie : secteur.getVoiesList()) {
                if (voie.getCotation() != null && !voie.getCotation().isEmpty()) {
                    cotations.add(cotationArrayList.indexOf(voie.getCotation()));
                }
            }
        }
        Collections.sort(cotations);
        if (cotations.size() == 0) {
            site.setCotationMax("");
        } else {
            site.setCotationMax(cotationArrayList.get(cotations.get(cotations.size() - 1)));
        }
        siteRepository.save(site);
    }

    @Override
    public Page<Site> filter(Pageable pageable, String selectRegion, String cotation, String isOfficiel,
                             String nombreSecteurs, String nombreVoies, String name) {
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        final QSite site = QSite.site;
        int inputSecteursInt = Integer.parseInt(nombreSecteurs);
        int inputVoiesInt = Integer.parseInt(nombreVoies);
        if (selectRegion.isEmpty() && cotation.isEmpty() && isOfficiel.equalsIgnoreCase("FALSE") &&
                nombreSecteurs.equals("0") && nombreVoies.equals("0") && name.isEmpty()) {
            return siteRepository.findAll(pageable);
        }
        if (!selectRegion.isEmpty()) {
            booleanBuilder.and(site.region.eq(selectRegion));
        }
        if (cotation != null && !cotation.isEmpty()) {
            booleanBuilder.and(site.cotationMax.eq(cotation));
        }
        if (isOfficiel != null && isOfficiel.equalsIgnoreCase("TRUE")) {
            booleanBuilder.and(site.official.isTrue());
        }
        if (name != null && !name.isEmpty()) {
            booleanBuilder.and(site.name.containsIgnoreCase(name));
        }
        if (inputSecteursInt != 0) {
            booleanBuilder.and(site.numbersSecteurs.eq(inputSecteursInt));
        }
        if (inputVoiesInt != 0) {
            booleanBuilder.and(site.numbersVoies.eq(inputVoiesInt));
        }
        return siteRepository.findAll(booleanBuilder, pageable);
    }

    @Override
    public Site modif(Site oldSite, Site newSite) {
        if (!newSite.getName().isEmpty() & !newSite.getName().equalsIgnoreCase(oldSite.getName())) {
            oldSite.setName(newSite.getName());
        }
        if (!newSite.getRegion().isEmpty() & !newSite.getRegion().equalsIgnoreCase(oldSite.getRegion())) {
            oldSite.setRegion(newSite.getRegion());
        }
        if (!newSite.getStreet().isEmpty() & !newSite.getStreet().equalsIgnoreCase(oldSite.getStreet())) {
            oldSite.setStreet(newSite.getStreet());
        }
        if (newSite.getCodePost() != oldSite.getCodePost()) {
            oldSite.setCodePost(newSite.getCodePost());
        }
        if (!newSite.getCity().isEmpty() & !newSite.getCity().equalsIgnoreCase(oldSite.getCity())) {
            oldSite.setCity(newSite.getCity());
        }
        if (!newSite.getDescription().isEmpty() & !newSite.getDescription().equalsIgnoreCase(oldSite.getDescription())) {
            oldSite.setDescription(newSite.getDescription());
        }
        return oldSite;
    }

    @Override
    public void setNumbersSecteurAndVoie(Site site) {
        int voies = 0;
        if (site.getSecteursList() != null) {
            for (Secteur secteur : site.getSecteursList()) {
                if (secteur.getVoiesList() != null) {
                    voies = voies + secteur.getVoiesList().size();
                }
            }
            site.setNumbersSecteurs(site.getSecteursList().size());
            site.setNumbersVoies(voies);
        }
        siteRepository.save(site);
    }
}
