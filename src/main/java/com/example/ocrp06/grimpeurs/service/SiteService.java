package com.example.ocrp06.grimpeurs.service;

import com.example.ocrp06.grimpeurs.entity.Site;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Set;

public interface SiteService {

    Site save(Site site);

    Site findByName(String name);

    Site getOne(Long siteId);

    Page<Site> findAll(Pageable pageable);

    List<Site> findAll();

    List<Site> findAll(Sort sort);

    Set<String> allRegions();

    void setCotationMax(Site site);

    Page<Site> filter(Pageable pageable, String selectRegion, String cotation, String isOfficiel, String inputSecteurs, String inputVoies, String name);

    void delete(Site site);

    Site modif(Site oldSite, Site newSite);

    void setNumbersSecteurAndVoie(Site site);
}
