package com.example.ocrp06.grimpeurs.service;

import com.example.ocrp06.grimpeurs.entity.Reservation;
import com.example.ocrp06.grimpeurs.entity.Topo;
import com.example.ocrp06.grimpeurs.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ReservationService {

    Reservation getOne(Long id);

    List<Reservation> findAll();

    Page<Reservation> findByUserRequesting(User requesting, Pageable pageable);

    Page<Reservation> findByUserOwnerTopo(User owner, Pageable pageable);

    Reservation save(Reservation reservation);

    void deleteByTopo(Topo topo);

    void setDismiss(Topo topo);

    void delete(Reservation reservation);

    List<Reservation> findByTopoAndUser(Topo topo, User user);

    boolean isExist(Topo topo, User user);

}
