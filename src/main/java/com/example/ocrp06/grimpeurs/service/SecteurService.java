package com.example.ocrp06.grimpeurs.service;

import com.example.ocrp06.grimpeurs.entity.Secteur;
import com.example.ocrp06.grimpeurs.entity.Site;

import java.util.List;

public interface SecteurService {

    Secteur save(Secteur secteur);

    Secteur findByName(String name);

    Secteur findByParent(Site site);

    List<Secteur> findAll();

    void delete(Secteur secteur);

    Secteur modify(Secteur oldSecteur, Secteur newSecteur);
}
