package com.example.ocrp06.grimpeurs;

import com.example.ocrp06.grimpeurs.config.MVCConfig;
import com.example.ocrp06.grimpeurs.config.SecurityConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class GrimpeursApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(GrimpeursApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(GrimpeursApplication.class, SecurityConfiguration.class, MVCConfig.class);
    }

}
