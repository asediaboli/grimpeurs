package com.example.ocrp06.grimpeurs.entity;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Secteur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Veuillez saisir du nom")
    @Length(max = 255, message = "Nom est trop long")
    private String name;

    @NotBlank(message = "Veuillez saisir de la description")
    @Length(max = 2048)
    private String description;

    @ManyToOne
    @JoinColumn(name = "site_id", nullable = false)
    private Site site;

    @CreatedDate
    private Date dateCreation;

    @LastModifiedDate
    private Date dateModified;

    @OneToMany(mappedBy = "secteur", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Voie> voiesList;

    public Secteur() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public List<Voie> getVoiesList() {
        return voiesList;
    }

    public void setVoiesList(List<Voie> voiesList) {
        this.voiesList = voiesList;
    }
}
