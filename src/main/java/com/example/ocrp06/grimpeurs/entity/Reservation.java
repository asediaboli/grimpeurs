package com.example.ocrp06.grimpeurs.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    //démandeur
    private User user;

    @ManyToOne
    @JoinColumn(name = "topo_id", nullable = false)
    private Topo topo;

    @Column(columnDefinition = "boolean default false")
    private boolean confirmed;

    @Column(columnDefinition = "boolean default false")
    private boolean dismiss;

    @Column(columnDefinition = "boolean default false")
    private boolean annuled;

    @CreatedDate
    private Date dateCreation;

    @LastModifiedDate
    private Date dateModified;

    public Reservation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Topo getTopo() {
        return topo;
    }

    public void setTopo(Topo topo) {
        this.topo = topo;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public boolean isDismiss() {
        return dismiss;
    }

    public void setDismiss(boolean dismiss) {
        this.dismiss = dismiss;
    }

    public boolean isAnnuled() {
        return annuled;
    }

    public void setAnnuled(boolean annuled) {
        this.annuled = annuled;
    }
}
