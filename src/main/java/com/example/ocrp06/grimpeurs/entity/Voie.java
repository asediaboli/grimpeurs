package com.example.ocrp06.grimpeurs.entity;


import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Voie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Veuillez saisir du nom")
    @Length(max = 255, message = "Nom est trop long")
    private String name;

    @ManyToOne
    @JoinColumn(name = "secteur_id", nullable = false)
    private Secteur secteur;

    @NotBlank(message = "Veuillez saisir de la cotation")
    private String cotation;

    @NotBlank(message = "Veuillez saisir de la description")
    @Length(max = 2046, message = "Description est trop long")
    private String description;

    @CreatedDate
    private Date dateCreation;

    @LastModifiedDate
    private Date dateModified;

    @OneToMany(mappedBy = "voie", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Longueur> longueurList;

    public Voie() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Secteur getSecteur() {
        return secteur;
    }

    public void setSecteur(Secteur secteur) {
        this.secteur = secteur;
    }

    public String getCotation() {
        return cotation;
    }

    public void setCotation(String cotation) {
        this.cotation = cotation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Longueur> getLongueurList() {
        return longueurList;
    }

    public void setLongueurList(List<Longueur> longueurList) {
        this.longueurList = longueurList;
    }
}
