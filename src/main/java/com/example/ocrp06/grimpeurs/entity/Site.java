package com.example.ocrp06.grimpeurs.entity;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Site {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Veuillez saisir du nom")
    @Length(max = 255, message = "Nom est trop long")
    private String name;

    @NotBlank(message = "Veuillez saisir du region")
    @Length(max = 150, message = "Region est trop long")
    private String region;

    @NotBlank(message = "Veuillez saisir de la rue")
    @Length(max = 255, message = "Rue est trop long")
    private String street;

    private int codePost;

    @NotBlank(message = "Veuillez saisir de la ville")
    @Length(max = 150, message = "Ville est trop long")
    private String city;

    @NotBlank(message = "Veuillez saisir de la description")
    @Length(max = 4096, message = "Description est trop long")
    private String description;

    @Column(columnDefinition = "boolean default false")
    private boolean official;
    private String cotationMax;

    @CreatedDate
    private Date dateCreation;

    @LastModifiedDate
    private Date dateModified;

    private Integer numbersComments = 0;
    private Integer numbersSecteurs = 0;
    private Integer numbersVoies = 0;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "createur_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "site", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Secteur> secteursList;

    @OneToMany(mappedBy = "site", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Comment> commentList;

    @OneToMany(mappedBy = "site", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Topo> topoList;

    public Site() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getCodePost() {
        return codePost;
    }

    public void setCodePost(int code_post) {
        this.codePost = code_post;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isOfficial() {
        return official;
    }

    public void setOfficial(boolean official) {
        this.official = official;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getCotationMax() {
        return cotationMax;
    }

    public void setCotationMax(String cotationMax) {
        this.cotationMax = cotationMax;
    }

    public List<Secteur> getSecteursList() {
        return secteursList;
    }

    public void setSecteursList(List<Secteur> secteursList) {
        this.secteursList = secteursList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getNumbersComments() {
        return numbersComments;
    }

    public void setNumbersComments(Integer numbersComments) {
        this.numbersComments = numbersComments;
    }

    public Integer getNumbersSecteurs() {
        return numbersSecteurs;
    }

    public void setNumbersSecteurs(Integer numbersSecteurs) {
        this.numbersSecteurs = numbersSecteurs;
    }

    public Integer getNumbersVoies() {
        return numbersVoies;
    }

    public void setNumbersVoies(Integer numbersVoies) {
        this.numbersVoies = numbersVoies;
    }
}
