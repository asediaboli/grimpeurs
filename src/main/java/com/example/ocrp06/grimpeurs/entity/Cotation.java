package com.example.ocrp06.grimpeurs.entity;


public enum Cotation {

    A3("3A"),
    B3("3B"),
    C3("3C"),
    A4("4A"),
    B4("4B"),
    C4("4C"),
    A5("5A"),
    B5("5B"),
    C5("5C"),
    A6("6A"),
    B6("6B"),
    C6("6C"),
    A7("7A"),
    B7("7B"),
    C7("7C"),
    A8("8A"),
    B8("8B"),
    C8("8C"),
    A9("9A"),
    B9("9B"),
    C9("9C");

    private String value;

    private Cotation(String value) {
        this.value = value;
    }

    public static Cotation fromString(String value) {
        if (value != null) {
            for (Cotation pt : Cotation.values()) {
                if (value.equalsIgnoreCase(pt.value)) {
                    return pt;
                }
            }
        }
        throw new IllegalArgumentException("No such value");
    }

    public String getValue() {
        return value;
    }

}
