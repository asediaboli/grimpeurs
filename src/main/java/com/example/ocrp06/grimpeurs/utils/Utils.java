package com.example.ocrp06.grimpeurs.utils;

import com.example.ocrp06.grimpeurs.entity.Cotation;

import java.util.ArrayList;
import java.util.Arrays;

public class Utils {

    static public ArrayList<String> cotationArrayList() {
        ArrayList<Cotation> cotations = new ArrayList<Cotation>(Arrays.asList(Cotation.values()));
        ArrayList<String> arrayList = new ArrayList<>();
        for (Cotation m : cotations) {
            arrayList.add(m.getValue());
        }
        return arrayList;
    }
}
